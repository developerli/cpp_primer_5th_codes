#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item total; // 用于归集相同isbn的交易
    // 读取第一笔交易数据
    if (std::cin >> total) {
        Sales_item trans; // 存储当前正在读取的交易数据
        while (std::cin >> trans) {
            // 加总相同isbn的交易数据
            if (total.isbn() == trans.isbn()) {
                total += trans;
            } else {
                // 打印某个isbn的交易总和
                std::cout << total << std::endl;
                total = trans;
            }
        }
        std::cout << total << std::endl;
    } else {
        // 没有输入数据
        std::cerr << "No data!" << std::endl;
        return -1;
    }
    return 0;
}
